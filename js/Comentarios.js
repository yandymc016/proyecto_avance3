
function validar() {
    var nombre = document.getElementById("name").value;
    var correo = document.getElementById("correo").value;
    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var comentarios = document.getElementById ("comentario").value;
    if ((nombre == "") || (correo == "") || (comentarios == "")) {
        alert("Es necesario rellenar los campos vacios");
        return false;
    }
    else{
        if( !expr.test(correo)){
            alert("El correo electronico usado no es valido")
            return false;
        }
        else{
            alert("El comentario fue enviado con exito");
            return true;
        }

    }

}
