
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var dataBase= null;
var registro = [

];

function database() {
    
  dataBase = indexedDB.open('encuestas', 1);
  
  dataBase.onupgradeneeded = function (e) {
      var active = dataBase.result;
      var object = active.createObjectStore("nombres", { keyPath : 'id', autoIncrement : true });
      object.createIndex('by_nombre', 'nombre', { unique : false });
      object.createIndex('by_correo', 'correo', { unique : true });
  };
  
  dataBase.onsuccess = function (e) {
    console.log('Database loaded');
  };

  dataBase.onerror = function (e) {
    console.log('Error loading database');
  };
}


function add() {
                
    var active = dataBase.result;
    var data = active.transaction(["nombres"], "readwrite");
    var object = data.objectStore("nombres");
    
    var request = object.put({
      nombre:document.querySelector('#Nombre').value,
      apellido:document.querySelector('#Ape').value,
      correo:document.querySelector('#correo').value,
      contraeña:document.querySelector('#Contra').value
    });
    
    request.onerror = function (e) {
        alert(request.error.name + '\n\n' + request.error.message);
    };
    
    data.oncomplete = function (e) {
        document.querySelector('#Nombre').value ='';
        document.querySelector('#Ape').value = '';
        document.querySelector('#correo').value = '';
        document.querySelector('#Contra').value = '';
        console.log('Object añadido');
    };
}


function Validar_R(){
    var Nombre= document.getElementById("Nombre").value;
   var Apellido= document.getElementById("Ape").value;
    var Correo= document.getElementById("correo").value;
    var Contraseña = document.getElementById("Contra").value;
    var RContraseña = document.getElementById("Contra1").value;
    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var cont = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  
    if ((Nombre.length == 0) || (Apellido.length == 0) || (Correo.length == 0) || (RContraseña.length == 0) || (Contraseña.length == 0)) {  //Comprueba campos vacios
        alert("Los campos no pueden quedar vacios");
        return false;
      }
    else{
        if ( !expr.test(Correo)){      //Comprueba el email tenga la sintaxis correcta
          alert("Error: La dirección de correo " + Usuario + " es incorrecta.");
          return false;
        }
        else{
          if( !cont.test(Contraseña)){    // comprueba que la contraseña este en el estandar de 8 digitos
            alert("La contraseña ingresada no es correcta");
            return false;
          }
          else{
            if(Contraseña != RContraseña){
              alert("las contraseñas no coinciden");
              return false;
            }
            else{
                alert("todo bien");
                add();
            }
          }
        }
      }
  
    }
  